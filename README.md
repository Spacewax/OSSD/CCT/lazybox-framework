# LazyBox Framework

is th framework being built, constructed, patched, glued...whatever for some of our projects.

A large portion of the **LazyBox Framework** is [inspired by this article](https://github.com/PatrickLouys/no-framework-tutorial). The excerpt below was a major inspiration for doing this.


*An excerpt of that article:*

>I saw a lot of people coming into the Stack Overflow PHP chatroom and asking if framework X is any good. Most of the time the answer was that they should just use PHP and not a framework to build their application. But many are overwhelmed by this and don't know where to start.
>
>So my goal with this is to provide an easy resource that people can be pointed to. In most cases a framework does not make sense and writing an application from scratch with the help of some third party packages is much, much easier than some people think.
>


### License
This is open sourced because we feel opening the source code benfits many.

A final License has not been decided yet.
We are deciding between MIT License, Apache License 2.0 or a BSD License.

For time being we will use the MIT License,
reserving the right to change. Change *WILL* happen before or during alpha testing.
