<?php
/**
*
* @copyright	http://www.spacewax.net/ The LazyBox Project
* @license		https://opensource.org/licenses/MIT MIT License (MIT)
* @package		core
* @since		  1.0
* @author		  hyperclock <hyperclock@spacewax.net>
* @version		1.0.x-dev
*
*/

declare(strict_types = 1);

require __DIR__ . '/../src/Bootstrap.php';
